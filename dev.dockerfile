ARG RUST_VERSION
FROM rust:${RUST_VERSION}-slim
RUN apt-get update && \
    apt-get -y install --no-install-recommends pkg-config git libssl-dev libpq-dev build-essential postgresql-client && \
    rustup component add rls rust-analysis rust-src rustfmt clippy && \
    cargo install sqlx-cli --no-default-features --features rustls,postgres && \
    apt-get autoremove -y && \
    apt-get clean -y && \
    rm -rf /var/lib/apt/lists/*