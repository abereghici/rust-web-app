ARG RUST_VERSION
FROM rust:${RUST_VERSION}-slim
RUN apt-get update && \
    apt-get -y install --no-install-recommends libpq-dev && \
    rustup target add wasm32-unknown-unknown && \
    cargo install cargo-watch && \
    cargo install --locked trunk